package com.example.examplefunctionaljavaspringbootformula;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Formula {
  private final int valueA;
  private final int valueB;
}
