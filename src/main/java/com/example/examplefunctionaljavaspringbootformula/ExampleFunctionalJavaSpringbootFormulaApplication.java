package com.example.examplefunctionaljavaspringbootformula;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class ExampleFunctionalJavaSpringbootFormulaApplication {
  private RestTemplate rest = new RestTemplate();

  public static void main(String[] args) {
    SpringApplication.run(
      ExampleFunctionalJavaSpringbootFormulaApplication.class,
      args
    );
  }

  @PostMapping("/formula")
  public int formula(@RequestBody Formula formula) {
    Integer constant = rest.getForObject(
      System.getenv("GITERVER_API_GATEWAY_PREFIX") +
      "/example_base_java_springboot_constant/constant",
      Integer.class
    );
    return formula.getValueA() * constant + formula.getValueB();
  }
}
